/**
 * base level export for all actions in the application
 */

//@TODO temporary, this should be an ENV var
export const PYTHON_API = process.env.REACT_APP_.PYTHON_API;

export * from '../Loading.redux/Loading.actions';
export * from '../IsError.redux/IsError.actions';
export * from '../Login.redux/Login.actions';
export * from '../LogOut.redux/LogOut.actions';
export * from '../AuthToken.redux/AuthToken.actions';
export * from '../SessionSlot.redux/SessionSlot.actions';
